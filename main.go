package main

import (
  "log"
  "sync/atomic"
  "net/http"
  "encoding/json"
)

type Metrics struct {
  HitsCount uint64
}

func main() {

  metrics := Metrics{}

  log.Println("starting server on port 8000")
  http.HandleFunc("/home", func(w http.ResponseWriter, r *http.Request) {
      atomic.AddUint64(&metrics.HitsCount, 1)
      encoder := json.NewEncoder(w)
      encoder.Encode(&metrics)
  })
  log.Println("error", http.ListenAndServe(":8000", nil))
}
